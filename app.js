let offset = 0;
const sliderLine  = document.querySelector('.slider-line');

function showSlides() {
    offset = offset + 400;
    if(offset > 1600) {
        offset = 0;
    }
    sliderLine.style.left = -offset + 'px';
}

setInterval(showSlides, 3000)